from pathmodel.pathmodel_wrapper import run_pathmodel

def main(args=None):
    run_pathmodel()

if __name__ == "__main__":
    main()
